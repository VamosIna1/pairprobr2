package com.example.pairprogramlogin

import java.io.Serializable

data class DataUser (
        val name:String,
        val email:String
        ):Serializable