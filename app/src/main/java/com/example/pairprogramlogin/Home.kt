package com.example.pairprogramlogin

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class Home: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_beranda)
        var tvBeranda = findViewById<TextView>(R.id.textViewHome)
        tvBeranda.text=intent.getStringExtra("name")
    }
}